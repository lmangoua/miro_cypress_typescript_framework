/// <reference types="cypress" />

import SignUpPage from '../../pageObjects/signUp.page';
import EmailConfirmPage from '../../pageObjects/emailConfirm.page';
import { name, email, password, signUp_url, checkYourEmail_label, getStarted_label, emailConfirm_url, signup, getStartedNow_label } from '../../fixtures/resources/webTestData';

const signUp = new SignUpPage();
const emailConfirm = new EmailConfirmPage();

//Setup Part
before(() => {

    signUp.navigateToUrl(signup);
    
    cy.log("Navigated to : " + signUp_url);
});

//Tear down Part
after(() => {
    
    cy.screenshot('Capturing the screenshot after Tear down');
    cy.log("****** Tear down ******");
});

describe('Sign Up To Miro - Test suite', () => {

    it('Sign Up - Test case', () => {

        signUp.validateSignUpPage(signUp_url).should('have.text', getStarted_label);
        signUp.enterName().type(name);
        signUp.enterEmail().type(email);
        signUp.enterPassword().type(password);
        signUp.iAgreeToMiroTerms().click();
        signUp.getStartedNow().contains(getStartedNow_label).click();
        emailConfirm.validateEmailConfirmPage(emailConfirm_url).should('have.text', checkYourEmail_label);

        cy.log("Signed Up Successfully");
    });
});