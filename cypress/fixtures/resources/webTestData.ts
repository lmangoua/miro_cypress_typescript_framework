/* Test data used for Web UI */
//url
export const signUp_url = 'https://miro.com/signup/';
export const signup = '/signup';
//sign up details
export const getStarted_label = 'Get started free today';
export const uuid = () => Cypress._.random(0, 1e6); //to generate random number
export const id = uuid();
export const name = `lionel${id}`;
export const email = `lionel.email${id}@gmail.com`;
export const password = `Password_${id}`;
export const getStartedNow_label = 'Get started now';
//email confirm page
export const emailConfirm_url = 'https://miro.com/email-confirm/';
export const checkYourEmail_label = 'Check your email';
