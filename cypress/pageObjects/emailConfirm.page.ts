/// <reference types="cypress" />

class EmailConfirmPage {

    validateEmailConfirmPage(value: string) {
        cy.url().should('be.equal', value);
        return cy.get('h1[class="signup__title-form"]');
    }
}

export default EmailConfirmPage