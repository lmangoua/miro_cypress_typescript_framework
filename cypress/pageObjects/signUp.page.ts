/// <reference types="cypress" />

class SignUpPage {

    navigateToUrl(value: string) {
        const url = value;
        cy.visit(url);
    }

    validateSignUpPage(value: string) {
        cy.url().should('be.equal', value);
        return cy.get('h1[class="signup__title-form signup__title-form--sign-up ab-signup-usa--title"]');
    }

    enterName() {
        return cy.get('input[name="signup[name]"]').clear();
    }

    enterEmail() {
        return cy.get('input[name="signup[email]"]').clear();
    }

    enterPassword() {
        return cy.get('input[name="signup[password]"]').clear();
    }

    iAgreeToMiroTerms() {
        return cy.xpath('(//span[contains(text(),"I agree to Miro")]/../label)[1]');
    }

    iAgreeToReceiveMiroNews() {
        return cy.xpath('//span[contains(text(),"I agree to receive Miro news and updates.")]/../label');
    }

    getStartedNow() {
        return cy.get('button[class="signup__submit"]');
    }
}

export default SignUpPage