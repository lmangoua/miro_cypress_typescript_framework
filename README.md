# Miro_Cypress_Project
This project is part of Miro QA Assessment to test Web application.

# Miro Assessment Automation Framework

## FEATURES ##

The **Miro Assessment Automation Framework** implements a 'Page Object Model' approach.

Page Object Model(POM) Framework has recently become a very popular test automation framework since it enables easy test maintenance 
and reduces the duplication of code ie. maximizes re-usabilty of code.
The main advantage of POM is that if the UI changes for any page, no tests need to be updated.  
Instead, we just need to change only the code within the page objects ie. only at one place.

This Automation framework allows you to run tests on almost any browser and OS. It also enables you to write all types of test(E2E, Integration tests, Unit tests).

It also provides the ability to run locally on Windows, Mac, Ubuntu with minimal changes to any config files.

## TOOLS USED ##

Visual Studio Code IDE(version 1.64.2), Cypress(version 9.4.1), Typescript(version 4.5.5), NodeJs(version 17.4.0), npm(version 6.14.8), Mac OS Monterey(version 12.2).

---

## PRE-REQUISITES ##

### 1. Install Visual Studio Code ###
Download (link: https://code.visualstudio.com/download) and install Visual Studio Code in your machine.

### 2. Install NodeJs ###
2.1 Run command on terminal

    $ brew install node

2.2 Verify NodeJs is installed
    
    $ node -v

2.3 Verify NPM is installed

    $ npm -v

### 3. Clone project from GitLab repository ###
3.1 Repository link: https://gitlab.com/bobeezy/miro_cypress_project

3.2 Clone project in local machine

    $ git clone https://gitlab.com/bobeezy/miro_cypress_project.git

---

## CYPRESS TYPESCRIPT FRAMEWORK STRUCTURE ##

### A) cypress ###
This is the main test folder found in the **Automation framework root folder** 
It contains the sub-folders:

**1. fixtures**
- contains /resources/webTestData.ts file where the test datas used throughout the project are stored

**2. integration**
- contains /specs/signUp.spec.ts file where we store the test cases

**3. pageObjects**
- contains /emailConfirm.page.ts and /signUp.page.ts files that are used to store the actions and locators of each page

**4. plugins**
- contains /index.js file that is used to make plugins or listeners configuration

**5. screenshots**
- contains the screenshots saved after each test execution

**6. support**
- contains /commands.js and /index.js files used to customise commands or reusable methods that are available for usage in all of the specs/test files

### B) node_modules ###
Is the heart of the cypress project. All the node packages are installed in the node_modules directory and will be available in all the test files

### C) cypress.json ###
Is used to store different configurations. E.g., timeout, base URL, test files, or any other configuration that we want to override for tweaking the behaviour of Cypress

### D) package.json ###
Is a file which contains the name and its version of all the dependencies that is required in our project

### E) package-lock.json ###
If the npm changes in the package.json or node_moduels tree, then it is automatically generated in the package-lock.json file

### F) tsconfig.json ###
This file specifies the root files and the compiler options required to compile the project

---

## EXECUTING TEST SUITE ##
1. Command to execute cypress test using Command Line Interface CLI (run headless test from terminal without opening browser)
    <br></br>
    From VS Code terminal, run CMD:

    $ npx cypress run --spec="./cypress/integration/specs/<specName>.spec.ts"

    e.g. $ npx cypress run --spec="./cypress/integration/specs/signUp.spec.ts"


2. Command to open cypress UI window
    <br></br>
    - From VS Code terminal, run CMD to open cypress window:

    $ npm run cypress:open
                      
    OR    
                 
    $ npm run cy:open

    - Then click the spec we want to run under /specs/:

    e.g. click "signUp.spec.ts"

---

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/bobeezy/miro_cypress_project.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/bobeezy/miro_cypress_project/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
